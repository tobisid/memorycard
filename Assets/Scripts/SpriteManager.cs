﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpriteManager : MonoBehaviour
{
    public static SpriteManager instance = null;
    public static List<Sprite> sprite_List =new List<Sprite>();
    public static bool downloaded = false;

    public string url;
    void Awake()
    {
        if(instance != null && instance !=this)
        {
            Destroy(this.gameObject);
            return;
        }

        instance = this;
       

        DontDestroyOnLoad(gameObject);
        StartCoroutine(getDataRequest());
    }


    public  IEnumerator getDataRequest()
    {
        UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
        }
        else
        {
            var downTex = JsonConvert.DeserializeObject<RootObject>(request.downloadHandler.text);


            
            foreach (Link l in downTex.links)
            {
                Debug.Log(l.link_url);
                StartCoroutine(processingJson(l.link_url));
            }
            downloaded = true;
        }
    }


    IEnumerator processingJson(string _url)
    {
        UnityWebRequest request = UnityWebRequest.Get(_url);
        DownloadHandler handle = request.downloadHandler;
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        {
            Texture2D t = new Texture2D(16,16);
            Sprite sp = null;
            if (t.LoadImage(handle.data))
            {
                sp = Sprite.Create(t, new Rect(0, 0, t.width, t.height),
                new Vector2(0.5f, 0.5f), 100);
            }
            if (sp != null)
            { 
                sprite_List.Add(sp);
            }
            
        }
    }

}


public class Link
{
    public string link_url { get; set; }
}

public class RootObject
{
    public List<Link> links { get; set; }
}
