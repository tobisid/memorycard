﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardBehavior : MonoBehaviour
{

    [SerializeField] private GameObject _imgCard;
    [SerializeField] private Controller sc;

    
    private int id_card;
    public int id_Card
    {
        set { id_card = value; }
        get { return id_card; }      
    }
    private void Start()
    {
        
        StartCoroutine(ShowCards());
    }

    IEnumerator ShowCards()
    {
        var rotateCoord = Quaternion.Euler(0, 0, 0);
        _imgCard.GetComponent<Animator>().SetTrigger("rotate");
        yield return new WaitForSeconds(1f);
        _imgCard.SetActive(false);
        _imgCard.transform.rotation = rotateCoord;
        yield return new WaitForSeconds(4f);
        _imgCard.SetActive(true);
    }


    public void OnMouseDown()
    {
        if (_imgCard.activeSelf && Controller.openable &&  Controller.gameStarted)
        {
            _imgCard.SetActive(false);

            sc.OpenCard(this);
        }
    }


    public void NewSprite(Sprite sprite,int index)
    {
        id_card = index;
        GetComponent<SpriteRenderer>().sprite = sprite;
    }
  

    public void Reset_Card()
    {
        _imgCard.SetActive(true);
    }


    public void DesctroyCard()
    {
        _imgCard.SetActive(false);
        gameObject.SetActive(false);
    }
}
