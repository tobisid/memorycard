﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpleshScene : MonoBehaviour
{
    [SerializeField] private GameObject leg;
    void Start()
    {
        leg.GetComponent<Animation>().Play();
        
    }


    private void Update()
    {
        if (SpriteManager.downloaded)
            StartNewScene();
    }
    public void StartNewScene()
    {
        StartCoroutine(LoadMainScene());
    }

    IEnumerator LoadMainScene()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(1);
    }
}
