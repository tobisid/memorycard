﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    public const int columns = 3, rows = 2;
    public const float offX = 6f, offY = 4.5f;

    [SerializeField] private CardBehavior card;
    [SerializeField] private Sprite default_img;
    [SerializeField] private TMP_Text scoreText;
    private int score_count;
    public int Score_Count
    {
        get { return score_count; }
        set
        {
            score_count = value;
            scoreText.text = "Cобранно пар: " + score_count.ToString();
        }
    }

    private List<Sprite> sprites= new List<Sprite>();
    private Vector3 pos= new Vector3();
    private CardBehavior card1;
    private CardBehavior card2;

    public static bool openable = false;
    public static bool gameStarted = false;

    void Start()
    {
        score_count = 0;
        sprites = SpriteManager.sprite_List;
        Debug.Log(sprites.Count);
        pos = card.transform.position;

        int[] id_sprite = { 0, 0, 1, 1, 2, 2 };

        id_sprite = this.mixArr(id_sprite);

        for(int i = 0; i < columns; i++)
        {
            for(int k = 0; k < rows; k++)
            {
                CardBehavior new_card;
                if(i==0 && k == 0)
                {
                    new_card = card;
                }

                else
                {
                    new_card = (CardBehavior)Instantiate(card);
                }

                int index = k * columns + i;
                int id = id_sprite[index];
                new_card.NewSprite(sprites[id], id);

                float x_pos = offX * i + pos.x;
                float y_pos = offY * k + pos.y;
                Vector3 new_pos = new Vector3(x_pos, y_pos, pos.z);
                new_card.transform.position = new_pos;
            }
        }

        StartCoroutine(StartingGame());

    }

    IEnumerator StartingGame()
    {
        yield return new WaitForSeconds(5f);
        gameStarted = true;
    }

    // Update is called once per frame
    void Update()
    { if (gameStarted)
        {
            if (card2 != null)
            {
                openable = false;
                card1.StopAllCoroutines();
                card2.StopAllCoroutines();
            }

            else openable = true;

            if (score_count == 3) StartCoroutine(Restarting());
        }
    }

    public void OpenCard(CardBehavior cardB)
    {
        if (card1 == null)
        {
            card1 = cardB;
        }
        else
        {
            card2 = cardB;
            StartCoroutine(Checking());
        }
    }


    IEnumerator Checking()
    {
        if(card1.id_Card == card2.id_Card)
        {
            Score_Count++;
            yield return new WaitForSeconds(1f);
            card1.DesctroyCard();
            card2.DesctroyCard();
        }
        else
        {
            yield return new WaitForSeconds(1f);
            card1.Reset_Card();
            card2.Reset_Card();
        }
        card1 = null;
        card2 = null;
        StopAllCoroutines();
    }

    IEnumerator Restarting()
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(0);
    }

    int[] mixArr(int[] arr)
    {
        int length = arr.Length;
        int[] array = (int[])arr.Clone();
        for(int i =0;i<length;i++)
        {
            int random_index = Random.Range(i, length);
            int temp = array[i];
            array[i] = array[random_index];
            array[random_index] = temp;
        }
        return array;
    }
}
